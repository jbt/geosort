#pragma once

#include <iosfwd>

namespace geosort{
namespace    geo {
    
    struct point
    {
        float lat_ = 0.f;
        float long_= 0.f;
        
        float operator-( point rhs ) const;
    };
    
    auto operator<<( std::ostream&, point const& ) -> std::ostream&;

}}
