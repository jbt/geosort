#include "point.hpp"
#include <cmath>
#include <iostream>

namespace g = geosort::geo;

auto g::operator<<( std::ostream& s, point const& p ) -> std::ostream&
{
    return s << '(' << p.lat_ << ',' << p.long_ << ')';
}

namespace {
    constexpr auto rad_per_degree = 2. * 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647093844 / 360.;
    constexpr auto R = 3'958.7613;//miles
}

auto g::point::operator-( point rhs ) const -> float
{
/*
Given the latitudes and longitudes of two places on earth, what is the (great circle) distance between them?

    The earth is (generally close enough to) a sphere, radius R; define the (surface) distance we seek as D.
    D=Ra where a is the angle (in radians, or in degrees*(2Pi/360)) between vectors R1 and R2 from the center of the earth to the two points.
    Since the dot product R1.R2=R2cosa, we can write D=Rcos-1(R1.R2/R2)
    Transform spherical to rectilinear coordinates: (R,lat,long)->(Rcos(lat)cos(long),Rcos(lat)sin(long),Rsin(lat)).
    So R1.R2/R2=cos(lat1)cos(long1)cos(lat2)cos(long2)+cos(lat1)sin(long1)cos(lat2)sin(long2)+sin(lat1)sin(lat2).
    D=R cos-1(cos(lat1)cos(long1)cos(lat2)cos(long2)+cos(lat1)sin(long1)cos(lat2)sin(long2)+sin(lat1)sin(lat2)).
    D=R cos-1(cos(long1-long2)cos(lat1)cos(lat2)+sin(lat1)sin(lat2)).
    Sanity check: On the equator (both latitudes zero) this says D=R(long1-long2) as expected.
    Sanity check: When the two longitudes are equal this says D=R(lat1-lat2) as expected.
*/
    auto& lhs = *this;
    auto lat1  = lhs.lat_ * rad_per_degree;
    auto long1 = lhs.long_* rad_per_degree;
    auto lat2  = rhs.lat_ * rad_per_degree;
    auto long2 = rhs.long_* rad_per_degree;
    //D=R cos-1(cos(long1-long2)cos(lat1)cos(lat2)+sin(lat1)sin(lat2)).
    return R * std::acos(std::cos(long1-long2)*std::cos(lat1)*std::cos(lat2)+std::sin(lat1)*std::sin(lat2));
}
