#include <list/entry.hpp>
#include <list/fixed_width.hpp>
#include <list/load.hpp>
#include <zip/locate.hpp>
#include <algorithm>
#include <iostream>

int main( int argc, char const* argv[] )
{
    if ( argc < 2 )
    {
        std::clog << "USAGE: geosort <reference_zip> [required_interest] [csv_file_to_sort...]\n";
        return EXIT_FAILURE;
    }
    using namespace geosort;
    auto ref = zip::locate( std::atoi(argv[1]) );
    std::clog << argv[1] << " is at " << ref << '\n';
    
    std::vector<list::entry> entries;
    std::string_view required_interest = ( argc > 2 ) ? argv[2] : "";
    auto load = [required_interest,&entries](char const*f){list::load(entries,f,required_interest);};
    std::for_each( std::next(argv,2), std::next(argv,argc), load );
    std::clog << "Loaded " << entries.size() << " entries.\n";
    
    for ( auto& etry : entries )
    {
        auto pos = zip::locate( etry.zip_ );
        etry.distance_ = pos - ref;
    }
    
    std::sort( entries.begin(), entries.end() );
    entries.erase( std::unique(entries.begin(), entries.end()), entries.end() );
    
    list::fixed_width( std::cout, entries );
}
