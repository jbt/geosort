#pragma once

#include <cstdint>

namespace geosort {
namespace zip {
    
    using code = std::int_least32_t;

}}
