#pragma once

#include <zip/code.hpp>
#include <geo/point.hpp>
#include <cstdint>

namespace geosort{
namespace    zip {

    geo::point locate( code );

}}
