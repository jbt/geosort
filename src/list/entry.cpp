#include "entry.hpp"
#include <cassert>

namespace l = geosort::list;

auto l::entry::operator<( entry const& rhs ) const -> bool
{
    if ( distance_ != rhs.distance_ ) {
        return distance_ < rhs.distance_;
    }
    if ( last_name_ != rhs.last_name_ ) {
        return last_name_ < rhs.last_name_;
    }
    return first_name_ < rhs.first_name_;
}
