#pragma once

#include <filesystem>
#include <vector>

namespace geosort{
namespace   list {
    
    class entry;
    
    using std::filesystem::path;
    
    auto load( std::vector<entry>& in_out, path csv_file, std::string_view required_interst ) -> std::size_t;
    
}}
