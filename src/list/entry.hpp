#pragma once

#include <zip/code.hpp>
#include <array>
#include <string>
#include <string_view>

namespace geosort {
namespace list {
    
    using namespace std::literals;

    class entry
    {
    public:
        std::string first_name_ = "First Name";
        std::string  last_name_ = "Last Name";
        std::string      phone_ = "Phone #";
        zip::code zip_ = {};
        std::int_least16_t distance_ = 0;
        
        bool operator<( entry const& rhs ) const;
        bool operator==(entry const& rhs ) const { return !(*this < rhs || rhs < *this); }
    };
    
}}
