#include "load.hpp"
#include <list/entry.hpp>
#include <csv/csv.h>
#include <boost/algorithm/string.hpp> //TODO dependency on boost
#include <iostream>//TODO should not access clog directly

namespace l = geosort::list;

auto l::load( std::vector<entry>& in_out, path csv_path, std::string_view interest ) -> std::size_t
{
    auto prev_size = in_out.size();
    io::CSVReader<4,io::trim_chars<>,io::double_quote_escape<',','"'>> reader{ csv_path.string() };
    reader.read_header(io::ignore_extra_column
        , "Contact Name"
        , "Phone"
        , "Postal Code"
        , "Volunteer Interests"
        );
    entry e;
    std::string name, interests;
    while ( reader.read_row(name, e.phone_, e.zip_, interests) )
    {
        auto comma = name.find( ',' );
        if ( comma < name.size() ) {
            e.first_name_.assign( name, comma + 1ul );
            e.last_name_.assign( name, 0ul, comma );
            boost::trim( e. last_name_ );
        } else {
            e.first_name_.assign( name );
        }
        boost::trim( e.first_name_ );
        if ( interests.empty() || boost::icontains(interests,interest) ) {
            in_out.push_back( e );
        } else {
            std::clog << name << " has no ZIP code?\n";
        }
    }
    return in_out.size() - prev_size;
}
