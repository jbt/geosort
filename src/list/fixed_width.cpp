#include "fixed_width.hpp"
#include <list/entry.hpp>
#include <iomanip>

namespace l = geosort::list;

namespace {
    std::string spaces;
    struct width
    {
        std::string l::entry::* pstr_;
        std::size_t size_;
        
        void expand_to( l::entry const& e )
        {
            auto sz = (e.*pstr_).size();
            if ( size_ < sz )
            {
                size_ = sz;
                if ( sz > spaces.size() ) {
                    spaces.resize( sz, ' ' );
                }
            }
        }
        void output( std::ostream& os, l::entry const& e )
        {
            auto s = e.*pstr_;
            os << s;
            os.write( spaces.c_str(), size_ - s.size() );
        }
    };
}

auto l::fixed_width( std::ostream& write_to, std::vector<entry> const& rows ) -> std::ostream&
{
    std::array dynamic_widths{
        width{ & entry::first_name_, 0ul }
      , width{ & entry:: last_name_, 0ul }
      , width{ & entry::     phone_, 0ul }
    };
    for ( auto& row : rows )
    {
        for ( auto& dw : dynamic_widths ) {
            dw.expand_to( row );
        }
    }
    for ( auto& dw : dynamic_widths ) {
        dw.output( write_to, entry{} ); //write the header row
    }
    write_to << '\n';
    for ( auto& row : rows )
    {
        for ( auto& dw : dynamic_widths ) {
            dw.output( write_to, row );
        }
        write_to << std::setw(6) << row.zip_ << std::setw(5) << row.distance_ << '\n';
    }
    return write_to;
}
