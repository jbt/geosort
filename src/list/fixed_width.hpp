#pragma once

#include <iosfwd>
#include <vector>

namespace geosort{
namespace list {
    
    class entry;
    
    auto fixed_width( std::ostream& write_to, std::vector<entry> const& rows ) -> std::ostream&;
    
}}
